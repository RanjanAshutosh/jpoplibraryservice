package com.jpop.LibraryService.models;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape=JsonFormat.Shape.STRING)
public class Book {

	private Long bookid;
	private String bookName;
	private String author;
	private String category;
	private String description;
	
	public Book() {	}

	public Book(Long bookid, String bookName, String author, String category, String description) {
		super();
		this.bookid = bookid;
		this.bookName = bookName;
		this.author = author;
		this.category = category;
		this.description = description;
	}

	public Long getBookid() {
		return bookid;
	}

	public void setBookid(Long bookid) {
		this.bookid = bookid;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
