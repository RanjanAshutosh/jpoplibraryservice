package com.jpop.LibraryService.servicesImpl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jpop.LibraryService.models.Book;
import com.jpop.LibraryService.services.LibraryBookService;

@Service
public class LibraryBookServiceImpl  implements LibraryBookService{
	
	@Autowired
    private RestTemplate restTemplate;
	
	@Value("${book.url}")
	private String bookUrl;
	
	@Override
	public List<Book> getAllBooks() {
	    return Arrays.asList(restTemplate.getForObject(bookUrl , Book[].class));
	  }
	
	@Override
	public Book getBook(Long id) {
		return restTemplate.getForObject(bookUrl + "/" + id, Book.class);
	}
	
	@Override
	public Book addBook(Book book) {
		return restTemplate.postForObject(bookUrl , book , Book.class);
	}
	
	@Override
	public Book updateBook(Long bookid, Book book) {
		 restTemplate.put(bookUrl + "/" + bookid , book);
		 return book;
	}
	
	@Override
	public void deleteBook(Long bookId) {
		 restTemplate.delete(bookUrl + "/" + bookId );
	}

}
