package com.jpop.LibraryService.servicesImpl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jpop.LibraryService.models.User;

@Service
public class LibraryUserServiceImpl {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${user.url}")
	private String userUrl;

	public List<User> getAllUsers() {
		return Arrays.asList(restTemplate.getForObject(userUrl, User[].class));
	}

	public User getUser(Long userId) {
		return restTemplate.getForObject(userUrl + "/" + userId, User.class);
	}

	public User addUser(User user) {
		return restTemplate.postForObject(userUrl, user, User.class);
	}

	public User updateUser(Long userId, User user) {
		restTemplate.put(userUrl + "/" + userId, user);
		return user;
	}
	
	public void deleteUser(Long userId) {
		restTemplate.delete(userUrl + "/" + userId );
	}
}
