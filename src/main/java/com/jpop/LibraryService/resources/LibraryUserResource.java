package com.jpop.LibraryService.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpop.LibraryService.models.User;
import com.jpop.LibraryService.servicesImpl.LibraryUserServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Library Service")
@RestController
@RequestMapping("/lib/users")
public class LibraryUserResource {

	@Autowired
	private LibraryUserServiceImpl libraryUserServiceImpl;
	
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved All Users"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	
	@GetMapping()
	public List<User> getAllUsers() {
		return libraryUserServiceImpl.getAllUsers();
	}
	
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved Users"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	
	@GetMapping("/{userId}")
	public ResponseEntity<User> getUser(@PathVariable Long userId) {
		User user = libraryUserServiceImpl.getUser(userId);
		if (user == null) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully added a User"),
			@ApiResponse(code = 400, message = "The resource you were trying to reach is not found") })
	
	@PostMapping()
	public ResponseEntity<User> addUser(@RequestBody User user) {
		return buildJsonResponse(libraryUserServiceImpl.addUser(user));
	}
	
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully updated a User"),
			@ApiResponse(code = 400, message = "The resource you were trying to reach is not found") })
	
	@PutMapping("/{userId}")
	public ResponseEntity<User> updateUser(@PathVariable Long userId, @RequestBody User user) {
		return buildJsonResponse(libraryUserServiceImpl.updateUser(userId, user));
	}
	
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted a User"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	
	@DeleteMapping("/{userId}")
	public ResponseEntity<String> deleteBook(@PathVariable Long userId) {
		User user = libraryUserServiceImpl.getUser(userId);

		if (user == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not find Resource to delete.");
		}
		libraryUserServiceImpl.deleteUser(userId);
		return ResponseEntity.status(HttpStatus.OK).body("Resource Deleted successfully!!");
	}
	
	
	/**
	 * @param quoteResponse
	 * @return
	 */
	private ResponseEntity<User> buildJsonResponse(User user) {
		ResponseEntity<User> response = null;
		if (user == null) {
			response = new ResponseEntity<>(user, HttpStatus.BAD_REQUEST);
		} else {
			response = new ResponseEntity<>(user, HttpStatus.CREATED);
		}
		return response;
	}
}
