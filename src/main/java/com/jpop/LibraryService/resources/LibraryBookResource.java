package com.jpop.LibraryService.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpop.LibraryService.models.Book;
import com.jpop.LibraryService.servicesImpl.LibraryBookServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Library Service")
@RestController
@RequestMapping("/lib/books")
public class LibraryBookResource {

	@Autowired
	private LibraryBookServiceImpl libraryBookServiceImpl;

	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved All books"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })

	@GetMapping()
	public List<Book> getAllBooks() {
		return libraryBookServiceImpl.getAllBooks();
	}

	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved a book"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })

	@GetMapping("/{bookId}")
	public ResponseEntity<Book> getBook(@PathVariable Long bookId) {
		Book book = libraryBookServiceImpl.getBook(bookId);
		if (book == null) {
			return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Book>(book, HttpStatus.OK);
	}

	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully added a book"),
			@ApiResponse(code = 400, message = "The resource you were trying to reach is not found") })

	@PostMapping()
	public ResponseEntity<Book> addBook(@RequestBody Book book) {
		return buildJsonResponse(libraryBookServiceImpl.addBook(book));
	}

	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully updated a book"),
			@ApiResponse(code = 400, message = "The resource you were trying to reach is not found") })

	@PutMapping("/{bookId}")
	public ResponseEntity<Book> updateBook(@PathVariable Long bookId, @RequestBody Book book) {
		return buildJsonResponse(libraryBookServiceImpl.updateBook(bookId, book));
	}

	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted a books"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })

	@DeleteMapping("/{bookId}")
	public ResponseEntity<String> deleteBook(@PathVariable Long bookId) {
		Book book = libraryBookServiceImpl.getBook(bookId);

		if (book == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Could not find Resource to delete.");
		}
		libraryBookServiceImpl.deleteBook(bookId);
		return ResponseEntity.status(HttpStatus.OK).body("Resource Deleted successfully!!");
	}

	/**
	 * @param quoteResponse
	 * @return
	 */
	private ResponseEntity<Book> buildJsonResponse(Book book) {
		ResponseEntity<Book> response = null;
		if (book == null) {
			response = new ResponseEntity<>(book, HttpStatus.BAD_REQUEST);
		} else {
			response = new ResponseEntity<>(book, HttpStatus.CREATED);
		}
		return response;
	}
}
