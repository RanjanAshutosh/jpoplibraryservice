package com.jpop.LibraryService.services;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import com.jpop.LibraryService.models.Book;

@Component
public class BookFeignClientFallback implements LibraryBookService {

	@Override
	public List<Book> getAllBooks() {
		return Collections.emptyList();
	}

	@Override
	public Book getBook(Long id) {
		return new Book((long) 0, "NA", "NA", "NA", "NA");
	}

	@Override
	public Book addBook(Book book) {
		return new Book((long) 0, "NA", "NA", "NA", "NA");
	}

	@Override
	public Book updateBook(Long bookid, Book book) {
		return new Book((long) 0, "NA", "NA", "NA", "NA");
	}

	@Override
	public void deleteBook(Long bookId) {

	}

}
