package com.jpop.LibraryService.services;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.jpop.LibraryService.models.Book;

@FeignClient(name = "BOOK-SERVICE", fallback = BookFeignClientFallback.class)
public interface LibraryBookService {
	
	@GetMapping("/books")
	public List<Book> getAllBooks() ;
	
	@GetMapping("/books/{book_id}")
	public Book getBook(Long id) ;
	
	@PostMapping("/books")
	public Book addBook(Book book) ;
	
	@PutMapping("/books/{book_id}")
	public Book updateBook(Long bookid, Book book);
	
	@DeleteMapping("/books/{book_id}")
	public void deleteBook(Long bookId) ;
}
