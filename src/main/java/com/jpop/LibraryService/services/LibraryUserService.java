package com.jpop.LibraryService.services;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.jpop.LibraryService.models.User;

@FeignClient(name = "USER-SERVICE", fallback = UserFeignClientFallback.class)
public interface LibraryUserService {

	@GetMapping("users")
	public List<User> getAllUsers();

	@GetMapping("users/{user_id}")
	public User getUser(Long userId);

	@PostMapping("users")
	public User addUser(User user);

	@PutMapping("users/{user_id}")
	public User updateUser(Long userId, User user);

	@DeleteMapping("users/{user_id}")
	public void deleteUser(Long userId);

}
