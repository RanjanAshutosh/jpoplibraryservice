package com.jpop.LibraryService.services;

import java.util.Collections;
import java.util.List;

import com.jpop.LibraryService.models.User;

public class UserFeignClientFallback implements LibraryUserService{

	@Override
	public List<User> getAllUsers() {
		 return Collections.emptyList();
	}

	@Override
	public User getUser(Long userId) {
		return new User((long) 0, "NA");
	}

	@Override
	public User addUser(User user) {
		return new User((long) 0, "NA");
	}

	@Override
	public User updateUser(Long userId, User user) {
		return new User((long) 0, "NA");
	}

	@Override
	public void deleteUser(Long userId) {
		
	}

}
